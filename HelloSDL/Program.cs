﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDL2;

class Program
{
    // SDL state:
    private IntPtr Renderer;
    private IntPtr Window;

    // Assets:
    private Texture BlockTexture;

    // Game state:
    private Random Randomness = new Random();
    private List<Block> Blocks = new List<Block>();

    // Constants:
    public static readonly int ScreenWidth = 1280;
    public static readonly int ScreenHeight = 720;

    static void Main(string[] args)
    {
        Program program = new Program();
        program.Start();
        program.Run();
    }

    public void Start()
    {
        // ======================================================================================
        // Find the correct working directory
        // ======================================================================================
        string rootDir = Directory.GetDirectoryRoot(".");
        while (!File.Exists("SDL2.dll"))
        {
            // If we reach the root directory without finding our DLLs, crash.
            string currentDir = Directory.GetCurrentDirectory();
            if (rootDir == currentDir)
            {
                throw new Exception("Unable to find required libraries.");
            }

            Directory.SetCurrentDirectory("..");
        }

        // ======================================================================================
        // Initialize SDL
        // ======================================================================================
        if (SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING) != 0)
        {
            throw new Exception("Failed to initialize SDL.");
        }

        Window = SDL.SDL_CreateWindow(
            "Hello SDL!",
            SDL.SDL_WINDOWPOS_CENTERED_DISPLAY(1),
            SDL.SDL_WINDOWPOS_CENTERED_DISPLAY(1),
            ScreenWidth,
            ScreenHeight,
            0);
        if (Window == IntPtr.Zero)
        {
            throw new Exception("Failed to create window.");
        }

        SDL.SDL_RendererFlags rendererFlags =
            SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED |
            SDL.SDL_RendererFlags.SDL_RENDERER_PRESENTVSYNC;
        Renderer = SDL.SDL_CreateRenderer(Window, -1, rendererFlags);
        if (Renderer == IntPtr.Zero)
        {
            throw new Exception("Failed to create renderer.");
        }

        // ======================================================================================
        // Load assets
        // ======================================================================================
        BlockTexture = Texture.CreateFromFile(Renderer, "Assets/block.png");
    }

    public void Run()
    {
        Time lastFrameStartTime = Time.Now();

        while (true)
        {
            // ======================================================================================
            // Measure the time elapsed between one frame and the next
            // ======================================================================================
            TimeDelta timeDelta;
            {
                Time now = Time.Now();
                timeDelta = now - lastFrameStartTime;
                lastFrameStartTime = now;
            }
            // Convert the high-precision time delta into a more convenient value.
            float delta = timeDelta.ToSeconds();

            // ======================================================================================
            // Process events
            // ======================================================================================
            SDL.SDL_Event evt;
            while (SDL.SDL_PollEvent(out evt) != 0)
            {
                if (evt.type == SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN)
                {
                    // When the mouse is clicked, create a block with a random color.
                    float x = evt.button.x;
                    float y = evt.button.y;
                    if (evt.button.button == SDL.SDL_BUTTON_LEFT)
                    {
                        BlockColor[] allColors = new BlockColor[]
                        {
                            BlockColor.Red,
                            BlockColor.Green,
                            BlockColor.Blue,
                        };

                        Blocks.Add(new Block
                        {
                            Center = new Vector2(x, y),
                            Color = allColors[Randomness.Next(0, allColors.Length)],
                        });
                    }
                }
                else if (evt.type == SDL.SDL_EventType.SDL_KEYDOWN)
                {
                    // When an arrow key is pressed, move all blocks.
                    Vector2 motion = Vector2.Zero;
                    if (evt.key.keysym.sym == SDL.SDL_Keycode.SDLK_LEFT)
                    {
                        motion = new Vector2(-32, 0);
                    }
                    else if (evt.key.keysym.sym == SDL.SDL_Keycode.SDLK_RIGHT)
                    {
                        motion = new Vector2(+32, 0);
                    }

                    if (motion != Vector2.Zero)
                    {
                        foreach (Block block in Blocks)
                        {
                            block.Center += motion;
                        }
                    }
                }
                else if (evt.type == SDL.SDL_EventType.SDL_QUIT)
                {
                    Environment.Exit(0);
                }
            }

            // ======================================================================================
            // Update game logic
            // ======================================================================================

            // Blocks fall:
            foreach (Block block in Blocks)
            {
                float fallRate = 64;
                // NOTE: The frame time-delta is used so that blocks move at the same speed regardless
                // of the current framerate.
                block.Center.Y += delta * fallRate;
            }

            // Delete blocks that have left the screen:
            Blocks.RemoveAll(block => block.Center.Y > ScreenHeight + 100);

            // ======================================================================================
            // Draw
            // ======================================================================================
            SDL.SDL_SetRenderDrawColor(Renderer, 0x22, 0x22, 0x22, 0x00);
            SDL.SDL_RenderClear(Renderer);

            foreach (Block block in Blocks)
            {
                if (block.Color == BlockColor.Red)
                {
                    SDL.SDL_SetTextureColorMod(BlockTexture.Handle, 0xFF, 0x44, 0x44);
                }
                else if (block.Color == BlockColor.Green)
                {
                    SDL.SDL_SetTextureColorMod(BlockTexture.Handle, 0x44, 0xFF, 0x44);
                }
                else if (block.Color == BlockColor.Blue)
                {
                    SDL.SDL_SetTextureColorMod(BlockTexture.Handle, 0x44, 0x44, 0xFF);
                }

                DrawTexture(
                    BlockTexture,
                    block.Center.X - BlockTexture.Width / 2,
                    block.Center.Y - BlockTexture.Height / 2);
            }

            SDL.SDL_RenderPresent(Renderer);
        }
    }

    void DrawTexture(Texture texture, float x, float y)
    {
        SDL.SDL_Rect dest;
        dest.x = (int)x;
        dest.y = (int)y;
        dest.w = texture.Width;
        dest.h = texture.Height;
        SDL.SDL_RenderCopy(Renderer, texture.Handle, IntPtr.Zero, ref dest);
    }
}

class Block
{
    public Vector2 Center;
    public BlockColor Color;
}

enum BlockColor
{
    Red,
    Green,
    Blue,
}
