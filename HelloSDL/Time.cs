﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDL2;

/// <summary>
/// A high-resolution timestamp.
/// </summary>
struct Time
{
    public readonly ulong Ticks;

    public Time(ulong ticks)
    {
        Ticks = ticks;
    }

    public static Time Now()
    {
        ulong ticks = SDL.SDL_GetPerformanceCounter();
        return new Time(ticks);
    }

    public static TimeDelta operator -(Time a, Time b)
    {
        return new TimeDelta(a.Ticks - b.Ticks);
    }
}

/// <summary>
/// A high-resolution timespan.
/// </summary>
struct TimeDelta
{
    public readonly ulong Ticks;

    public TimeDelta(ulong ticks)
    {
        Ticks = ticks;
    }

    public float ToSeconds()
    {
        ulong frequency = SDL.SDL_GetPerformanceFrequency();
        double seconds = (double)Ticks / frequency;
        return (float)seconds;
    }
}
