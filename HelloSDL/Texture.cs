﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDL2;

class Texture
{
    public readonly IntPtr Handle;
    public readonly int Width;
    public readonly int Height;

    private Texture(IntPtr handle, int width, int height)
    {
        Handle = handle;
        Width = width;
        Height = height;
    }

    public static Texture CreateFromFile(IntPtr renderer, string filename)
    {
        IntPtr handle = SDL_image.IMG_LoadTexture(renderer, filename);
        if (handle == IntPtr.Zero)
        {
            throw new Exception("Failed to load texture.");
        }

        uint format;
        int access, width, height;
        SDL.SDL_QueryTexture(handle, out format, out access, out width, out height);

        return new Texture(handle, width, height);
    }
}
